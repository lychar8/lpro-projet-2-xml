<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="2.0" id="p2"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="text" encoding="ISO-8859-1"/>
	<xsl:strip-space elements="*"/>
	<xsl:variable name="eol" select="'&#10;'" as="xsd:string"/>

	<xsl:template match="comics">
		<xsl:apply-templates select="people"/>
	</xsl:template>

	<xsl:template match="people">
		<xsl:text>All the people we know the birth date :</xsl:text>
		<xsl:value-of select="$eol"/>
		<xsl:value-of select="$eol"/>
		<xsl:for-each select="./person">
			<xsl:sort select="@born" data-type="number" order="ascending"/>
			<xsl:apply-templates select="."/>
		</xsl:for-each>	
    </xsl:template>
	<xsl:template match="person">
		<xsl:if test="./@born">		
			<xsl:value-of select="@lastname"/><xsl:text> (</xsl:text>
			<xsl:value-of select="@firstname"/><xsl:text>) born : </xsl:text>
			<xsl:value-of select="@born"/>
			<xsl:if test="./@dead">
				<xsl:text> dead: </xsl:text>							
				<xsl:value-of select="@dead"/>
			</xsl:if>
			<xsl:if test="./@pseudonym">
				<xsl:text> [</xsl:text>							
				<xsl:value-of select="@pseudonym"/>
				<xsl:text>]  </xsl:text>
			</xsl:if>
			<xsl:value-of select="$eol"/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
