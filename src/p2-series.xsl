<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="2.0" id="p2"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" encoding="ISO-8859-1"/>
<xsl:strip-space elements="*"/>
<xsl:variable name="eol" select="'&#10;'" as="xsd:string"/>


<xsl:template match="comics">
	<xsl:apply-templates select ="collections"/>
</xsl:template>

<xsl:template match="collections">
	<xsl:apply-templates select ="series"/>
</xsl:template>

<xsl:template match="series">
	<xsl:value-of select="$eol"/>
	<xsl:text>     serie: </xsl:text>
	<xsl:value-of select="@name"/>
	<xsl:value-of select="$eol"/>
	<xsl:value-of select="$eol"/>
	<xsl:apply-templates select="item"/>

</xsl:template>

<xsl:template match="item">
<xsl:text>     </xsl:text>
<xsl:value-of select="normalize-space(.)"/>
		<xsl:text> published in </xsl:text>
		<xsl:value-of select="@year"/>
		<xsl:text>, </xsl:text>
		<xsl:if test="@additional='true'">
			<xsl:text>H, </xsl:text>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="./@scriptwriter">
				<xsl:text>S, </xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="./@artist">
				<xsl:text>A, </xsl:text>
			</xsl:when>
		</xsl:choose>
	<xsl:value-of select="$eol"/>
</xsl:template>
</xsl:stylesheet>
